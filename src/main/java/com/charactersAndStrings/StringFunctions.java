package main.java.com.charactersAndStrings;

import java.util.ArrayList;
import java.util.List;

public class StringFunctions {
    public int determineLengthOfShortestWord(String string){
        if (string.equals("")){
            return 0;
        }
        string = string.trim();
        int result;
        String[] words = string.split("\\s*[.,:;!? ]+\\s*");
        result = words[0].length();
        for (int i = 1; i < words.length; i++){
            if (result > words[i].length()){
                result = words[i].length();
            }
        }
        return result;
    }

    public String[] replaceLast3CharsWith$(String[] wordsArray, int length){
        for (int i = 0; i < wordsArray.length; i++){
            if (wordsArray[i].length() == length){
                if (wordsArray[i].length() < 3){
                    int arrLength = wordsArray[i].length();
                    wordsArray[i] = "";
                    for (int j = 0; j < arrLength; j++){
                        wordsArray[i] = wordsArray[i].concat("$");
                    }
                }
                else{
                    wordsArray[i] = wordsArray[i].substring(0, wordsArray[i].length() - 3);
                    wordsArray[i] = wordsArray[i].concat("$$$");
                }
            }
        }
        return  wordsArray;
    }

    public String addSpacesAfterPunctuation(String string){
        char[] chars = string.toCharArray();
        List<Integer> indexNeedSpace = new ArrayList<>();
        for (int i = 0; i < chars.length; i++){
            if (chars[i] == '.' || chars[i] == ',' || chars[i] == ';' || chars[i] == ':' || chars[i] == '?' ||
                    chars[i] == '!'){
                if (i != chars.length - 1){
                    if (chars[i + 1] != ' '){
                        indexNeedSpace.add(i + 1);
                    }
                }
            }
        }
        for (int i = indexNeedSpace.size() - 1; i >= 0; i--){
            int index = (int) indexNeedSpace.get(i);
            chars = addChar(chars, ' ', index);
        }
        String result = new String(chars);
        return result;
    }

    private char[] addChar(char[] chars, char charToAdd, int index){
        char[] newChars = new char[chars.length + 1];
        int charsIndex = 0;
        for (int i = 0; i < newChars.length; i++){
            if (i != index){
                newChars[i] = chars[charsIndex];
                charsIndex++;
            }
            else{
                newChars[i] = charToAdd;
            }
        }
        return newChars;
    }

    public String leaveOneInstanceOfWord(String string){
        string = string.trim();
        String[] words = string.split("\\s*[.,:;!? ]+\\s*");
        String[] singleWords = new String[0];
        int[] wordsUse = new int[0];

        for (int i = 0; i < words.length; i++){
            boolean isAlreadyUsed = false;
            for (int j = 0; j < singleWords.length; j++){
                if(singleWords[j].equals(words[i])){
                    wordsUse[j] += 1;
                    isAlreadyUsed = true;
                }
            }
            if(!isAlreadyUsed){
                singleWords = addString(singleWords, words[i]);
                wordsUse = addInt(wordsUse, 1);
            }
        }
        for (int i = 0; i < singleWords.length; i++){
            if (wordsUse[i] > 1){
                for (int j = 1; j < wordsUse[i]; j++){
                    int indexOfFirst = string.indexOf(singleWords[i], 0);
                    int indexToStartSearch = indexOfFirst + 1;
                    int indexOfRepeat = string.indexOf(singleWords[i], indexToStartSearch);
                    int repeatLength = singleWords[i].length();
                    string = removePartOfString(string, indexOfRepeat, repeatLength);
                }
            }
        }

        return string;
    }

    private String[] addString(String[] strings, String string){
        String[] newStrings = new String[strings.length + 1];
        for (int i = 0; i < strings.length; i++){
            newStrings[i] = strings[i];
        }
        newStrings[newStrings.length - 1] = string;
        return newStrings;
    }

    private int[] addInt(int[] ints, int number){
        int[] newInts = new int[ints.length + 1];
        for (int i = 0; i < ints.length; i++){
            newInts[i] = ints[i];
        }
        newInts[newInts.length - 1] = number;
        return newInts;
    }

    public int countWordsNumber(String string){
        if (string.equals("\\s*") || string.equals("")){
            return 0;
        }
        string = string.trim();
        int result;
        String[] words = string.split("\\s*[.,:;!? ]+\\s*");
        result = words.length;
        return result;
    }

    public String removePartOfString(String string, int startIndex, int length){
        if (startIndex < 0 || startIndex >= string.length() || length > string.length() - startIndex){
            return "Incorrect input";
        }
        String newString = "";
        if (startIndex != 0){
            newString = newString.concat(string.substring(0, startIndex));
        }
        if (length != string.length() - startIndex){
            newString = newString.concat(string.substring(startIndex + length, string.length()));
        }
        return newString;
    }

    public String flipString(String string){
        String[] stringSymbols = string.split("");
        String newString = "";
        for (int i = stringSymbols.length - 1; i >= 0; i--) {
            newString = newString.concat(stringSymbols[i]);
        }
        return newString;
    }

    public String deleteLastWord(String string){
        if (string.equals("")){
            return "";
        }
        string = string.trim();
        String tempStr = flipString(string);

        int punctuationIndex = 0;
        for (int i = 0; i < tempStr.length(); i++){
            String value = String.valueOf(tempStr.charAt(i));
            if (value.matches("[,.!?;:]")){
                punctuationIndex++;
            }
            else{
                break;
            }
        }
        String punctuation = tempStr.substring(0, punctuationIndex);

        int spaceIndex = -1;
        for (int i = 0; i < tempStr.length(); i++){
            if(tempStr.charAt(i) == ' '){
                spaceIndex = i;
                break;
            }
        }
        if (spaceIndex == -1){
            return string;
        }
        else{
            tempStr = tempStr.substring(spaceIndex + 1);
        }
        tempStr = tempStr.trim();
        tempStr = punctuation + tempStr;

        string = flipString(tempStr);
        string = string.trim();
        return string;
    }
}
