package main.java.com.charactersAndStrings;

public class Main {
    public static void main(String[] args) {
        StringOutputMethods stringOutputMethods = new StringOutputMethods();
//        stringOutputMethods.showEnglishAlphabetLowerCase();
//        stringOutputMethods.showEnglishAlphabetUpperCase();
//        stringOutputMethods.showRussianAlphabetLowerCase();
//        stringOutputMethods.showDigits();
//        stringOutputMethods.showAsciiTablePrintableRange();

        ConversionFunctions conversionFunctions = new ConversionFunctions();
//        System.out.println(conversionFunctions.convertIntToString(-324));
//        System.out.println(conversionFunctions.convertDoubleToString(-324.754));
//        System.out.println(conversionFunctions.convertStringToInt("-0002324"));
//        System.out.println(conversionFunctions.convertStringToDouble("-2314.576"));

        StringFunctions stringFunctions = new StringFunctions();
//        System.out.println(stringFunctions.flipString("It was not difficult task"));
//        System.out.println(stringFunctions.removePartOfString("Remove a part of this string", 6, 10));
//        String[] strings = {"ertyer", "fekewmfkw", "wewffw", "wefeew", "eew2fw", "er", "", "", "eq", "hfg45y"};
//        String[] newStrings = stringFunctions.replaceLast3CharsWith$(strings, 2);
//        for (String str : newStrings){
//            System.out.println(str);
//        }
//        System.out.println(stringFunctions.determineLengthOfShortestWord(" ewqeer,  wrew, rewr r3wefs;: were"));
//        System.out.println(stringFunctions.deleteLastWord("dwefw erew rwe rwrew"));
//        System.out.println(stringFunctions.addSpacesAfterPunctuation("wer,rwr.rwet..twe r?"));
//        System.out.println(stringFunctions.leaveOneInstanceOfWord("hello hell hell hello hello hell hell hello"));
    }
}
