package main.java.com.charactersAndStrings;

import java.math.BigDecimal;

public class ConversionFunctions {
    private static final char ZERO  = '0';
    private static final char ONE   = '1';
    private static final char TWO   = '2';
    private static final char THREE = '3';
    private static final char FOUR  = '4';
    private static final char FIVE  = '5';
    private static final char SIX   = '6';
    private static final char SEVEN = '7';
    private static final char EIGHT = '8';
    private static final char NINE  = '9';
    private static final char POINT  = '.';
    private static final char MINUS  = '-';
    private static final char[] digitChars = {ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE};

    public String convertIntToString(int number){
        String result = "";
        int numberCopy;
        int[] digits;
        if (number == 0){
            return "0";
        }
        int digitsNumber = 1;
        if (number < 0){
            result = result.concat("-");
        }
        number = Math.abs(number);
        numberCopy = number;
        while (true){
            numberCopy /= 10;
            if (numberCopy == 0){
                break;
            }
            else{
                digitsNumber++;
            }
        }
        digits = new int[digitsNumber];
        for (int i = 0; i < digitsNumber; i++){
            int digit = number % 10;
            number /= 10;
            digits[i] = digit;
        }
        for (int i = digitsNumber - 1; i >= 0; i--){
            char charDigit = (char)(digits[i] + 48);
            String stringDigit = String.valueOf(charDigit);
            result = result.concat(stringDigit);
        }
        return result;
    }

    public String convertDoubleToString(double number) {
        String result = "";

        if (number == 0) {
            return "0";
        }
        if (number < 0){
            result = result.concat("-");
        }
        number = Math.abs(number);

        int leftPart = (int) number;
        String leftPartConverted = convertIntToString(leftPart);
        result = result.concat(leftPartConverted);
        result = result.concat(String.valueOf(POINT));

        int rightPart = ((int)(number * 100000))  % 100000;
        String rightPartConverted = convertIntToString(rightPart);
        int zeroLength = 5 - rightPartConverted.length();
        for (int i = 0; i < zeroLength; i++){
            result = result.concat(String.valueOf(ZERO));
        }

        result = result.concat(rightPartConverted);
        return result;
    }

    public int convertStringToInt(String number){
        if (number == null || number == ""){
            System.out.println("Incorrect input");
            return -1;
        }

        int result = 0;
        char[] numberChar = number.toCharArray();

        boolean isMinus = false;
        if (numberChar[0] == MINUS){
            isMinus = true;
            char[] tempChar = new char[numberChar.length - 1];
            for (int i = 1; i < numberChar.length; i++){
                tempChar[i - 1] = numberChar[i];
            }
            numberChar = tempChar;
        }

        for (int i = 0; i < numberChar.length; i++){
            boolean isDigit = false;
            for (int j = 0; j < digitChars.length; j++){
                if (numberChar[i] == digitChars[j]){
                    isDigit = true;
                }
            }
            if (!isDigit){
                System.out.println("Incorrect input!!!");
                return -1;
            }
        }

        int[] digitInts = new int[numberChar.length];
        for (int i = 0; i < numberChar.length; i++){
            for (int j = 0; j < digitChars.length; j++){
                if (digitChars[j] == numberChar[i]){
                    digitInts[i] = j;
                }
            }
        }

        for (int i = 0; i < digitInts.length; i++){
            result += digitInts[i] * Math.pow(10, digitInts.length - 1 - i);
        }

        if (isMinus){
            result *= -1;
        }

        return result;
    }

    public double convertStringToDouble(String number){
        if (number == null || number.equals("")){
            System.out.println("Incorrect input");
            return -1;
        }

        double result;
        char[] numberChar = number.toCharArray();

        boolean isMinus = false;
        if (numberChar[0] == MINUS){
            isMinus = true;
            char[] tempChar = new char[numberChar.length - 1];
            for (int i = 1; i < numberChar.length; i++){
                tempChar[i - 1] = numberChar[i];
            }
            numberChar = tempChar;
        }
        number = new String(numberChar);

        int pointNumber = 0;
        int pointIndex = 0;
        for (int i = 0; i < numberChar.length; i++){
            if (numberChar[i] == POINT){
                pointNumber++;
                pointIndex = i;
                continue;
            }
            boolean isDigit = false;
            for (int j = 0; j < digitChars.length; j++){
                if (numberChar[i] == digitChars[j]){
                    isDigit = true;
                }
            }
            if (!isDigit){
                System.out.println("Incorrect input!!!");
                return -1;
            }
        }
        if (pointNumber > 1){
            System.out.println("Incorrect input!!!");
            return -1;
        }
        else if (pointNumber == 0){
            result = convertStringToInt(number);
        }
        else if (pointIndex == 0){
            System.out.println("Incorrect input!!!");
            return -1;
        }
        else{
            double leftDouble = 0;
            double rightDouble = 0;
            String leftPart = number.substring(0, pointIndex);
            leftDouble = convertStringToInt(leftPart);

            if (pointIndex != number.length() - 1){
                String rightPart = number.substring(pointIndex + 1);
                rightDouble = convertStringToInt(rightPart);
                rightDouble /= Math.pow(10, rightPart.length());
            }
            result = leftDouble + rightDouble;
        }

        if (isMinus){
            result *= -1;
        }

        return result;
    }
}
