package main.java.com.charactersAndStrings;

public class StringOutputMethods {
    public void showEnglishAlphabetLowerCase(){
        char englishLetterLow;
        for (int i = 97; i < 123; i++){
            englishLetterLow = (char) i;
            System.out.print(englishLetterLow);
        }
    }

    public void showEnglishAlphabetUpperCase(){
        char englishLetterUp;
        for (int i = 65; i < 91; i++){
            englishLetterUp = (char) i;
            System.out.print(englishLetterUp);
        }
    }

    public void showRussianAlphabetLowerCase(){
        char russianLetterLow;
        for (int i = 0x430; i <= 0x44F; i++){
            russianLetterLow = (char) i;
            System.out.print(russianLetterLow);
        }
    }

    public void showDigits(){
        char digit;
        for (int i = 48; i < 58; i++){
            digit = (char) i;
            System.out.print(digit);
        }
    }

    public void showAsciiTablePrintableRange(){
        char charAscii;
        for (int i = 33; i < 127; i++){
            charAscii = (char) i;
            System.out.print(charAscii);
        }
        for (int i = 161; i < 256; i++){
            charAscii = (char) i;
            System.out.print(charAscii);
        }
    }
}
