package test.java.com.charactersAndStrings;


import main.java.com.charactersAndStrings.ConversionFunctions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ConversionFunctionsTest {
    ConversionFunctions cut = new ConversionFunctions();

    static Arguments[] convertIntToStringTestArgs(){
        return new Arguments[]{
          Arguments.arguments(3756, "3756"),
          Arguments.arguments(-76, "-76"),
          Arguments.arguments(0, "0"),
        };
    }

    static Arguments[] convertDoubleToStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments(23, "23.00000"),
                Arguments.arguments(124.00340, "124.00340"),
                Arguments.arguments(-12.7030456, "-12.70304"),
                Arguments.arguments(0.0, "0"),
        };
    }

    static Arguments[] convertStringToIntTestArgs(){
        return new Arguments[]{
                Arguments.arguments("234", 234),
                Arguments.arguments("-76503", -76503),
                Arguments.arguments("0", 0)
        };
    }

    static Arguments[] convertStringToDoubleTestArgs(){
        return new Arguments[]{
                Arguments.arguments("123.3450", 123.345),
                Arguments.arguments("-3452.076", -3452.076),
                Arguments.arguments("78065", 78065),
                Arguments.arguments("0", 0),
        };
    }

    @ParameterizedTest
    @MethodSource("convertIntToStringTestArgs")
    public void convertIntToStringTest(int number, String expected){
        String actual = cut.convertIntToString(number);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("convertDoubleToStringTestArgs")
    public void convertDoubleToStringTest(double number, String expected){
        String actual = cut.convertDoubleToString(number);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("convertStringToIntTestArgs")
    public void convertStringToIntTest(String number, int expected){
        int actual = cut.convertStringToInt(number);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("convertStringToDoubleTestArgs")
    public void convertStringToDoubleTest(String number, double expected){
        double actual = cut.convertStringToDouble(number);
        Assertions.assertEquals(expected, actual);
    }
}
