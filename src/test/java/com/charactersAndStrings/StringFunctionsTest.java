package test.java.com.charactersAndStrings;

import main.java.com.charactersAndStrings.StringFunctions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class StringFunctionsTest {
    StringFunctions cut = new StringFunctions();
    static Arguments[] determineLengthOfShortestWordTestArgs(){
        return new Arguments[]{
                Arguments.arguments("cat", 3),
                Arguments.arguments("father,  sequence; method : cave", 4),
                Arguments.arguments("", 0),
                Arguments.arguments(" rat:  ", 3),
        };
    }

    static Arguments[] replaceLast3CharsWith$TestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"cat", "dog", "camel", "fish", "pig", "panel"}, 5,
                        new String[]{"cat", "dog", "ca$$$", "fish", "pig", "pa$$$"}),
                Arguments.arguments(new String[]{"cat", "dog", "camel", "fish", "pig"}, 3,
                        new String[]{"$$$", "$$$", "camel", "fish", "$$$"}),
                Arguments.arguments(new String[]{"cat", "do", "go", "fish", "at"}, 2,
                        new String[]{"cat", "$$", "$$", "fish", "$$"}),
                Arguments.arguments(new String[]{"cat", "do", "a", "fish", "f"}, 1,
                        new String[]{"cat", "do", "$", "fish", "$"}),
                Arguments.arguments(new String[]{}, 6,
                        new String[]{}),
        };
    }

    static Arguments[] addSpacesAfterPunctuationTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Cat,dog,rat or cow?Make choice!", "Cat, dog, rat or cow? Make choice!"),
                Arguments.arguments("", ""),
                Arguments.arguments("What is going on???", "What is going on? ? ?"),
                Arguments.arguments("Animals:dog,cat,cow.Not animals:eagle,stork.",
                        "Animals: dog, cat, cow. Not animals: eagle, stork."),
        };
    }

    static Arguments[] leaveOneInstanceOfWordTestArgs(){
        return new Arguments[]{
                Arguments.arguments("hello hi hi hello", "hello hi  "),
                Arguments.arguments("", ""),
                Arguments.arguments("hello hi", "hello hi"),
                Arguments.arguments("hello hi goodbye goodbye hi goodbye", "hello hi goodbye   "),
        };
    }

    static Arguments[] countWordsNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments("What a day!", 3),
                Arguments.arguments("That cat is not black", 5),
                Arguments.arguments("", 0)
        };
    }

    static Arguments[] removePartOfStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Hello, friend!!!", 5, 8, "Hello!!!"),
                Arguments.arguments("Why?", 0, 4, ""),
                Arguments.arguments("What?", -4, 4, "Incorrect input"),
                Arguments.arguments("Attention!!!", 2, 14, "Incorrect input"),
        };
    }

    static Arguments[] flipStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Hello!", "!olleH"),
                Arguments.arguments("How are you?", "?uoy era woH"),
                Arguments.arguments("", "")
        };
    }

    static Arguments[] deleteLastWordTestArgs(){
        return new Arguments[]{
                Arguments.arguments("What a day!", "What a!"),
                Arguments.arguments("How are you?", "How are?"),
                Arguments.arguments("this is a cat", "this is a"),
                Arguments.arguments("", ""),
        };
    }

    @ParameterizedTest
    @MethodSource("determineLengthOfShortestWordTestArgs")
    public void determineLengthOfShortestWordTest(String string, int expected){
        int actual = cut.determineLengthOfShortestWord(string);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("replaceLast3CharsWith$TestArgs")
    public void replaceLast3CharsWith$Test(String[] words, int charNumber, String[] expected){
        String[] actual = cut.replaceLast3CharsWith$(words, charNumber);
        Assertions.assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("addSpacesAfterPunctuationTestArgs")
    public void addSpacesAfterPunctuationTest(String string, String expected){
        String actual = cut.addSpacesAfterPunctuation(string);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("leaveOneInstanceOfWordTestArgs")
    public void leaveOneInstanceOfWordTest(String string, String expected){
        String actual = cut.leaveOneInstanceOfWord(string);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("countWordsNumberTestArgs")
    public void countWordsNumberTest(String string, int expected){
        int actual = cut.countWordsNumber(string);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("removePartOfStringTestArgs")
    public void removePartOfStringTest(String string, int startIndex, int length, String expected){
        String actual = cut.removePartOfString(string, startIndex, length);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("flipStringTestArgs")
    public void flipStringTest(String string, String expected){
        String actual = cut.flipString(string);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("deleteLastWordTestArgs")
    public void deleteLastWordTest(String string, String expected){
        String actual = cut.deleteLastWord(string);
        Assertions.assertEquals(expected, actual);
    }
}
